package TaskPlanner;
import HappyFamily.structure.model.DayOfWeek;

import java.util.*;

public class Schedule {
    private final Map<String, ArrayList<String>> schedule = new HashMap<>();

    private final DayOfWeek[] days = new DayOfWeek[]{DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY};

    public void prepareSchedule(){
        for (DayOfWeek day : days) {
            schedule.put(String.valueOf(day), new ArrayList<>());
        }
    }

    public void fillWithMockData(){
        String[] mockedActivities = new String[]{"learn new technology", "play the guitar", "learn to dance", "read a book", "go shopping", "watch a movie", "play games"};
        int counter = 0;
        for (Map.Entry<String, ArrayList<String>> entry:schedule.entrySet()) {
            entry.setValue(new ArrayList<>(List.of(mockedActivities[counter])));
        }
    }

    public void printSchedule(){
        for ( Map.Entry<String, ArrayList<String>> set: schedule.entrySet()
             ) {
            System.out.println(set.getKey() + " = "
                    + set.getValue());
        }
    }

    public void printSchedule(String day){

        for (Map.Entry<String, ArrayList<String>> set: schedule.entrySet()
             ) {
            if(set.getKey().equalsIgnoreCase(day)){
                System.out.printf("Your tasks for %s: %s\n", day, set.getValue());
                break;
            }
        }

//        for (String[] row: schedule
//             ) {
//            if(row[0].toLowerCase().equals(day.toLowerCase())){
//                System.out.printf("Your tasks for %s: %s\n", day, row[1]);
//                break;
//            }
//        }
    }

    public void addActivityToSchedule(String selectedDay, ScheduleActivity activity){
        if(!schedule.containsKey(selectedDay)){
            return;
        }
        schedule.get(selectedDay).add(activity.toString());
//        for (int i = 0; i < schedule.length; i++) {
//            String day = schedule[i][0];
//            if(day.equals(selectedDay)){
//                schedule[i][1] += schedule[i][1].length() > 0? String.format("; %s", activity.toString()): activity.toString();
//                System.out.println("Done");
//                break;
//            }
//        }
    }

    public boolean match(String strToMatch, DayOfWeek[] arrayOfWords){
        for (DayOfWeek day:arrayOfWords
             ) {
            if(day.name().toLowerCase().equals(strToMatch)){
                return true;
            }

        }
        return false;
    }

    public void changeActivityforDay(String selectedDay, ScheduleActivity activity){
        schedule.get(selectedDay).clear();
        schedule.get(selectedDay).add(activity.toString());
//        for (int i = 0; i < schedule.length; i++) {
//            String day = schedule[i][0];
//            if(day.equals(selectedDay)){
//                schedule[i][1] = activity.toString();
//                System.out.println("Done");
//                break;
//            }
//        }
    }

    public String parseStringForDay(String str){
        for (DayOfWeek day: days
             ) {
            if(str.toLowerCase().contains(day.name().toLowerCase())){
                return day.name();
            }
        }
        return "";
    }

    public void scheduleServiceBasic(){
        Scanner scanner = new Scanner(System.in);
        String day;
        while (true){
            System.out.println("Please, input the day of the week:");

            day = scanner.next().toLowerCase().trim();
            if(day.equalsIgnoreCase("exit")){
                break;
            }

            while(!match(day, days)){
                if(match(day, days)){
                    day = scanner.next();
                    break;
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                    day = scanner.next().toLowerCase().trim();
                }
            }

            printSchedule(day);


        }
        scanner.close();
    }

    public void scheduleServiceAdvanced(){
        Scanner scanner = new Scanner(System.in);
        String userInput, userActivity;
        while (true){
            System.out.println("Please, input the day of the week:");

            userInput = scanner.nextLine().toLowerCase().trim();

            if(userInput.equalsIgnoreCase("exit")){
                break;
            }

            while(!match(userInput, days)){
                if(match(userInput, days)){
                    userInput = scanner.nextLine();
                    break;
                } else if((userInput.contains("change") || userInput.contains("reschedule")) && parseStringForDay(userInput).length()>0){
                    System.out.printf("Please, input new tasks for %s\n", parseStringForDay(userInput));
                    userActivity = scanner.nextLine();
                    changeActivityforDay(parseStringForDay(userInput), new ScheduleActivity(userActivity));
                    break;
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                    userInput = scanner.nextLine().toLowerCase().trim();
                }
            }

            printSchedule(userInput);


        }
        scanner.close();
    }

    @Override
    public String toString() {
        prepareSchedule();
        return "Schedule{" +
                "schedule=" + schedule +
                '}';
    }
}
