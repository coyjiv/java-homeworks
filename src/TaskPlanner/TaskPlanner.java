package TaskPlanner;

import java.util.Scanner;

public class TaskPlanner {
    public void start() {
        Scanner scanner = new Scanner(System.in);
        String choice;

        System.out.println("Welcome to the schedule app");
        System.out.println("Which version do you want to check? Default is advanced");
        while(!scanner.hasNext()){
            System.out.println("Something's wrong. Try again");
            scanner.next();
        }
        choice = scanner.next();

        if(choice.equals("default")){
            Schedule schedule = new Schedule();
            schedule.prepareSchedule();
            schedule.fillWithMockData();
            schedule.scheduleServiceBasic();
        } else {
            Schedule schedule = new Schedule();
            schedule.prepareSchedule();
            schedule.fillWithMockData();
            schedule.scheduleServiceAdvanced();
        }
        scanner.close();
    }

}
