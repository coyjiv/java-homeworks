package TaskPlanner;

public class ScheduleActivity {
    String name;
    String description;
    String status;

    public ScheduleActivity(String name, String description, String status){
        this.name = name;
        this.description = description;
        this.status = status;
    }
    public ScheduleActivity(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString(){
        return description==null || status==null? String.format("%s\n", name): String.format("%s:%s - status : %s\n", name, description, status);
    }

    public ScheduleActivity getActivity(){
        return this;
    }
}
