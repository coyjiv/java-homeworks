import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    private static final int FIELD_SIZE = 6;

    private static final Random random = new Random();
    public static void main() {
        System.out.println("Hi, this is a second homework, area shooting!");

        Scanner scanner = new Scanner(System.in);

        System.out.println("Which version would you like to play?");

        String version = scanner.nextLine();

        if(Objects.equals(version, "default")){
            base();
        }
        else{
            advanced();
        }
        scanner.close();

    }
    private static void prettyPrintMap(String[][] array){
        for (String[] row:array
        ) {
            for (String element:row
            ) {
                System.out.print(element + " | ");
            }
            System.out.println();
        }
    }
    private static String[][] createMap(){
        String[][] field = new String[FIELD_SIZE][FIELD_SIZE];

        for (int i = 0; i < field[0].length; i++) {
            field[0][i] = String.valueOf(i);
        }
        for (int j = 0; j < field.length; j++) {
            field[j][0] = String.valueOf(j);
        }
        for (int k = 1; k < field.length; k++) {
            for (int i = 1; i < field[k].length; i++) {
                field[k][i] = "-";
            }
        }
        return field;
    }

    private static int[] generateMarkCoords(){
        int xCoordinate = random.nextInt(FIELD_SIZE - 1) + 1;
        int yCoordinate = random.nextInt(FIELD_SIZE - 1) + 1;

        return new int[]{xCoordinate, yCoordinate};
    }

    private static boolean checkTheShots(String[][] array){
        int counter = 0;
        for (String[] line:array
             ) {
            for (String element:line
                 ) {
                if(element.equals("x")){
                    counter++;
                }
            }
        }
        return counter != 3;
    }

    private static boolean checkTheShot(String[][] array){
        int counter = 0;
        for (String[] line:array
        ) {
            for (String element:line
            ) {
                if(element.equals("x")){
                    counter++;
                }
            }
        }
        return counter != 1;
    }

    private static int[][] generateLineCoords(int x, int y, boolean horizontal){
        if(horizontal){
            return new int[][]{
                    {y, x},
                    {y, x + 1},
                    {y, x + 2}
            };
        } else{
            return new int[][]{
                    {y, x},
                    {y + 1, x},
                    {y + 2, x}
            };
        }
    }
    private static void base(){
        String[][] field = createMap();

        int[] markCoordinates = generateMarkCoords();

        System.out.println("All set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);

        int userInputX = 0, userInputY = 0;

        System.out.println("You need find a dot in 5x5 map.");
        System.out.println("You will be asked to enter the the row and then the column where you would like to shoot");
        System.out.println("After each iteration, I will print the map with the marking \"*\" of the place where you've shot");

        do{
            System.out.print("Enter the row : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputY = scanner.nextInt();
                    break;
                }
            }

            while (userInputY<=0 || userInputY>=FIELD_SIZE){
                System.out.println("You can't chose the field outside the map.");
                System.out.print("Try again. Enter the row : ");
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputY = scanner.nextInt();
                }
            }

            System.out.println("Great, now enter the column : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputX = scanner.nextInt();
                    break;
                }
            }

            while (userInputX<=0 || userInputX>=FIELD_SIZE){
                System.out.println("You can't chose the field outside the map.");
                System.out.print("Try again. Enter the column : ");
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputX = scanner.nextInt();
                }
            }

            if(userInputY == markCoordinates[0] && userInputX == markCoordinates[1]) {
                field[userInputY][userInputX] = "x";
            } else {
                field[userInputY][userInputX] = "*";
            }

            prettyPrintMap(field);

        } while (checkTheShot(field));

        scanner.close();

        System.out.println("You have won!");

    }

    private static void advanced(){
        int xCoordinate = random.nextInt(FIELD_SIZE - 3) + 1;
        int yCoordinate = random.nextInt(FIELD_SIZE - 3) + 1;

        boolean horizontal = random.nextBoolean();

        int[][] lineCoords = generateLineCoords(xCoordinate, yCoordinate, horizontal);

        String[][] field = createMap();

        System.out.println("All set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);

        int userInputX = 0, userInputY = 0;

        System.out.println("You need find a vertical or a horizontal line 3X1");
        System.out.println("You will be asked to enter the the row and then the column where you would like to shoot");
        System.out.println("After each iteration, I will print the map with the marking \"*\" of the place where you've shot");

        do{
            System.out.print("Enter the row : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputY = scanner.nextInt();
                    break;
                }
            }

            while (userInputY<=0 || userInputY>=FIELD_SIZE){
                System.out.println("You can't chose the field outside the map.");
                System.out.print("Try again. Enter the row : ");
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputY = scanner.nextInt();
                }
            }

            System.out.println("Great, now enter the column : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputX = scanner.nextInt();
                    break;
                }
            }

            while (userInputX<=0 || userInputX>=FIELD_SIZE){
                System.out.println("You can't chose the field outside the map.");
                System.out.print("Try again. Enter the column : ");
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    userInputX = scanner.nextInt();
                }
            }

            boolean checkIntercepting = Arrays.equals(lineCoords[0], new int[]{userInputY, userInputX}) || Arrays.equals(lineCoords[1], new int[]{userInputY, userInputX}) || Arrays.equals(lineCoords[2], new int[]{userInputY, userInputX});

                if(checkIntercepting){
                    field[userInputY][userInputX] = "x";
                } else {
                    field[userInputY][userInputX] = "*";
                }


            prettyPrintMap(field);

        } while (checkTheShots(field));

        scanner.close();

        System.out.println("You have won!");
    }
}
