import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Numbers {
    public static void main() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the Numbers (1) homework!");
        System.out.println("Do you want to check advanced version or default? (Advanced/default)");

        if(!scanner.hasNextLine()){
            System.out.println("This is not a string!");
            System.exit(0);
        }

        String option = scanner.next();

        if ("default".equals(option)) {
            base();
            System.out.println();
        } else {
            advanced();
            System.out.println();
        }

        scanner.close();

    }

    public static void base(){
        System.out.println("DEFAULT");
        int randomNum = new Random().nextInt(100+1);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Hi, if you want to play the game, you should enter your name :)");
        System.out.print("My name is: ");

        String name = scanner.next();

        while (name.length()<3){
            System.out.println("I don't think that's a real name. Try again.");
            System.out.print("My name is: ");
            name = scanner.next();
        }

        System.out.println("Let the game begin! ");


        int guess;

        do{
            System.out.println("Try to guess the number from 1 to 100!");

            System.out.print("Your number is : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    guess = scanner.nextInt();
                    break;
                }
            }


            if (randomNum>guess){
                System.out.println("Your number is too small. Please, try again.");
            }
            else{
                System.out.println("Your number is too big. Please, try again.");
            }


        }
        while (guess != randomNum);

        System.out.printf("Congratulations, %s!", name);
        scanner.close();

    }
    private static void advanced(){
        System.out.println("ADVANCED");

        String[][] questions = {
            {"The year when WWII start?", "1939"},
                {"The year when first iPhone was released?", "2007"},
                {"Windows first release year","1985"},
                {"The year of release HTML", "1991"},
                {"The year of release Java", "1996"},
                {"The year of C release", "1978"},
                {"The year of React release (JS framework)", "2013"}
        };

        int[] choicesArr = new int[0];




        String[] randomQuestion = questions[new Random().nextInt(questions.length+1)] ;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Hi, if you want to play the game, you should enter your name :)");
        System.out.print("My name is: ");

        String name = scanner.next();

        while (name.length()<3){
            System.out.println("I don't think that's a real name. Try again.");
            System.out.print("My name is: ");
            name = scanner.next();
        }

        System.out.println("Let the game begin! ");


        int guess;

        do{
            System.out.println(randomQuestion[0]);

            System.out.print("Your answer is : ");

            while (true){
                if(!scanner.hasNextInt()){
                    System.out.println("Well, that's not a number :) Try again.");
                    scanner.next();
                }
                else{
                    guess = scanner.nextInt();
                    int[] newChoises = new int[choicesArr.length + 1];
                    for (int i = 0; i < choicesArr.length; i++) {
                        newChoises[i] = choicesArr[i];
                    }
                    choicesArr = newChoises;
                    choicesArr[choicesArr.length - 1] = guess;
                    break;
                }
            }


            if (parseInt(randomQuestion[1])>guess){
                System.out.println("Your number is too small. Please, try again.");
            }
            else{
                System.out.println("Your number is too big. Please, try again.");
            }


        }
        while (guess != parseInt(randomQuestion[1]));

        System.out.printf("Congratulations, %s! ", name);

        System.out.println("Your guesses are: ");
        Arrays.sort(choicesArr);
        System.out.print("[");
        for (int i =choicesArr.length -1; i>=0;i--){
            System.out.print(choicesArr[i]);
            if(i != 0){
                System.out.print(", ");
            }
        }
        System.out.print("]");
        scanner.close();

    }
}
