package HappyFamily;

import HappyFamily.structure.controller.FamilyController;
import HappyFamily.structure.dao.CollectionFamilyDao;
import HappyFamily.structure.model.*;
import HappyFamily.structure.service.FamilyService;

public class Main {
    public static void main(String[] args) {

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.menu();
    }
}
