package HappyFamily;

import HappyFamily.structure.model.*;
import TaskPlanner.Schedule;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class HappyFamilyTest {
    @Test
    public void testEmptyHumanToString() {
        Human human = new Human();
        assertEquals("Human{name='null', surname='null', dateOfBirth=01/01/1970, iq=0, schedule=null}", human.toString());
    }
    @Test
    public void testHumanToString(){
        Human human = new Human("TestName", "TestSurname", 100, 100, new Schedule());
        assertEquals("Human{name='TestName', surname='TestSurname', dateOfBirth=01/01/1970, iq=100, schedule=Schedule{schedule={WEDNESDAY=[], MONDAY=[], THURSDAY=[], SUNDAY=[], TUESDAY=[], FRIDAY=[], SATURDAY=[]}}}", human.toString());
    }
    @Test
    public void testEmptyPetToString() {
        Dog pet = new Dog();
        assertEquals("DOG{nickname='null', age=0, trickLevel=0, habits=null}", pet.toString());
    }
    @Test
    public void testPetToString() {
        DomesticCat cat = new DomesticCat( "Masya", 3, 999,  new HashSet<>(List.of("arr")));
        assertEquals("DOMESTIC_CAT{nickname='Masya', age=3, trickLevel=999, habits=[arr]}", cat.toString());
    }
    @Test
    public void testDeleteChildRemovesChild(){
        Woman mother =  new Woman();
        Man father = new Man();

        Family family = new Family(mother, father);

        Human child = new Human();

        family.addChild(child);

        List<Human> emptyChildren = new ArrayList<>();

        family.deleteChild(child);

        assertEquals(emptyChildren, family.getChildren());
    }
    @Test
    public void testDeleteChildIgnoresNotExististedChild(){
        Woman mother = new Woman();
        Man father = new Man();

        Family family = new Family(mother, father);

        Human child = new Human("test","ggf", 29000393);
        Human fakeChild = new Human("faker", "fakersur", 210390129);

        family.addChild(child);
        family.deleteChild(fakeChild);


        assertEquals(1, family.getChildren().size());
        assertEquals(
                "[Human{name='test', surname='ggf', dateOfBirth=01/01/1970, iq=0, schedule=null}]",
                family.getChildren().toString());
    }
    @Test
    public void testAddChildIncreasesLengthOfChildren(){
        Woman mother = new Woman();
        Man father = new Man();

        Family family = new Family(mother, father);

        Human child = new Human("test","ggf", 2000);

        int oldSize = family.getChildren().size();

        family.addChild(child);

        int newSize = family.getChildren().size();

        System.out.println(oldSize);

        List<Human> newListOfChildren = new ArrayList<>();
        newListOfChildren.add(child);

        assertEquals(1, newSize);
        assertNotEquals(oldSize, newSize);
        assertEquals(family.getChildren().toString(), newListOfChildren.toString());
        assertEquals(family, family.getChildren().get(0).getFamily());

    }
    @Test
    public void tesCountFamilyReturnsCorrectCount(){
        Woman mother = new Woman();
        Man father = new Man();

        Family family = new Family(mother, father);

        Human child = new Human("test","ggf", 2000);

        assertEquals(2,family.countFamily());

        family.addChild(child);

        assertEquals(3, family.countFamily());
    }
    @Test
    public void testDescribePet() {
        Dog dog = new Dog("Steve", 5, 60, new HashSet<>(List.of("Playing with ball", "Sleeping")));
        Family family = new Family(new Woman(), new Man());
        family.addPet(dog);

        Human child = new Human();
        child.setFamily(family);

        child.describePet();

        // Проверьте вывод в консоль
    }

    @Test
    public void testGreetPet() {
        Dog dog = new Dog("Steve", 5, 60, new HashSet<>(List.of("Playing with ball", "Sleeping")));
        Family family = new Family( new Woman(), new Man());
        family.addPet(dog);

        Human child = new Human();
        child.setFamily(family);

        child.greetPet();


    }

    @Test
    public void testFeedPet() {
        Dog dog = new Dog("Steve", 5, 60, new HashSet<>(List.of("Playing with ball", "Sleeping")));
        Family family = new Family( new Woman(), new Man());
        family.addPet(dog);

        Human child = new Human();
        child.setFamily(family);

        child.feedPet(true);
        child.feedPet(false);


    }

    @Test
    public void testSetPet() {
        Family family = new Family(new Woman(), new Man());
        DomesticCat cat = new DomesticCat("Whiskers");

        family.addPet(cat);

        assertEquals(cat, family.getPet());
    }

    @Test
    public void testDeleteChildByIndex() {
        Woman mother = new Woman();
        Man father = new Man();
        Family family = new Family(mother, father);
        Human child = new Human();

        family.addChild(child);
        family.deleteChild(0);

        assertEquals(0, family.getChildren().size());
    }

    @Test
    public void testPetMethods() {
        Dog dog = new Dog("Steve", 5, 60, new HashSet<>(List.of("Playing with ball", "Sleeping")));

        dog.eat();
        dog.respond();


    }

    @Test
    public void testFoulMethod() {
        DomesticCat cat = new DomesticCat("Whiskers", 3, 50, new HashSet<>(List.of("Chasing birds", "Sleeping")));

        cat.foul();


    }

    @Test
    public void testDogMethods() {
        Dog dog = new Dog("Steve", 5, 60, new HashSet<>(List.of("Playing with ball", "Sleeping")));

        dog.respond();


    }

    @Test
    public void testDomesticCatMethods() {
        DomesticCat cat = new DomesticCat("Whiskers", 3, 50, new HashSet<>(List.of("Chasing birds", "Sleeping")));

        cat.respond();


    }
}
