package HappyFamily.structure.service;

import HappyFamily.structure.model.*;

import java.util.List;
import java.util.Scanner;

public interface IFamilyService {
    boolean editFamilyByIndex(Scanner scanner, int index);

    List<Family> getAllFamilies();

    void displayAllFamilies();

    List<Family> getFamiliesBiggerThan(int memberCount);

    List<Family> getFamiliesLessThan(int memberCount);

    int countFamiliesWithMemberNumber(int memberCount);

    void createNewFamily(Man father, Woman mother);

    boolean deleteFamilyByIndex(int index);

    Family bornChild(Family family, String menName, String womenName);

    Family adoptChild(Family family, Human child);

    boolean deleteAllChildrenOlderThan(int age);

    int count();

    Family getFamilyById(int index);

    List<Pet> getPets(int index);

    void addPet(int familyIndex, Pet pet);



}
