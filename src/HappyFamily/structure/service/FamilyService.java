package HappyFamily.structure.service;

import HappyFamily.structure.model.*;
import HappyFamily.structure.dao.FamilyDao;
import HappyFamily.structure.utils.ConsoleUtils;
import HappyFamily.structure.utils.FamilyOverflowException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FamilyService implements IFamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao){
        this.familyDao = familyDao;
    }

    @Override
    public boolean editFamilyByIndex(Scanner scanner, int index) {
        String menu = """
                1. Born child
                2. Adopt child
                3. Return to the main menu
                """;
        System.out.println(menu);
        System.out.print("Your option is : ");
        int option = ConsoleUtils.handleInputedNumber(scanner, "The provided option is not a number");
        while(true){
            switch (option){
                case 1:
                    System.out.println("Enter child's man name");
                    String manName = ConsoleUtils.handleInputedString(scanner, "This is not a valid string");
                    System.out.println("Enter child's girl name");
                    String womanName = ConsoleUtils.handleInputedString(scanner, "This is not a valid string");
                    System.out.println("Congratulations, child was born");
                    bornChild(getFamilyById(index), manName, womanName);
                    break;
                case 2:
                    System.out.println("Enter child's name");
                    String childName = ConsoleUtils.handleInputedString(scanner, "This is not a valid string");
                    System.out.println("Enter child's surname");
                    String childSurname = ConsoleUtils.handleInputedString(scanner, "This is not a valid string");
                    System.out.println("Enter child's birth year");
                    int childBirthYear = ConsoleUtils.handleInputedYear(scanner, "This is not a valid year.");
                    System.out.println("Enter child's birth year");
                    int childIq= ConsoleUtils.handleInputedIq(scanner, "This is not a valid IQ.");
                    adoptChild(getFamilyById(index), new Human(childName, childSurname, childBirthYear, childIq));
                    System.out.println("Congratulations, child was adopted");
                    break;
                case 3:
                    return false;
                default:
                    System.out.println("The provided option doesn't exist");
                    System.out.println(menu);
                    System.out.print("Your option is : ");
                    option = ConsoleUtils.handleInputedNumber(scanner, "The provided option is not a number");
            }
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    @Override
    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(System.out::println);
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int memberCount) {
        return familyDao.getAllFamilies().stream().filter(el-> el.countFamily()>memberCount).toList();
    }

    @Override
    public List<Family> getFamiliesLessThan(int memberCount) {
        return familyDao.getAllFamilies().stream().filter(el-> el.countFamily()<memberCount).toList();
    }

    public List<Family> getFamiliesEqualsTo(int memberCount) {
        return familyDao.getAllFamilies().stream().filter(el-> el.countFamily()==memberCount).toList();
    }

    @Override
    public int countFamiliesWithMemberNumber(int memberCount) {
        return familyDao.getAllFamilies().stream().filter(el-> el.countFamily()==memberCount).toList().size();
    }

    @Override
    public void createNewFamily(Man father, Woman mother) {
        Family newFamily = new Family(mother, father);
        familyDao.addFamily(newFamily);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        if(index<familyDao.getAllFamilies().size() && index>0){
            familyDao.getAllFamilies().remove(index);
            return true;
        }
        return false;
    }

    @Override
    public Family bornChild(Family family, String menName, String womenName) {
        family.getMother().bornChild(family, womenName, menName,  LocalDate.now().getYear());
        if(family.countFamily()>99){
            throw new FamilyOverflowException("The family can't be more than 99", family.countFamily());
        }
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).addChild(child);
        if(family.countFamily()>99){
            throw new FamilyOverflowException("The family can't be more than 99", family.countFamily());
        }
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThan(int age) {
        familyDao.getAllFamilies()
                .forEach(family -> {
                    List<Human> updatedChildren = family.getChildren().stream()
                            .filter(child -> LocalDate.now().getYear() - child.getBirthDate() <= age)
                            .collect(Collectors.toList());
                    family.setChildren(updatedChildren);
                });
        return true;
    }

    @Override
    public int count() {
        return familyDao.getAllFamilies().size();
    }

    @Override
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    @Override
    public List<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPets();
    }


    @Override
    public void addPet(int familyIndex, Pet pet) {
        familyDao.getFamilyByIndex(familyIndex).addPet(pet);
    }

    public void setFamilies(List<Family> families){
        familyDao.setFamilies(families);
    }

    public void loadExternalData(String filepath) {
        familyDao.loadExternalData(filepath);
    }

    public void saveExternalData(String filepath) throws IOException {
        familyDao.saveExternalData(filepath);
    }
}
