package HappyFamily.structure.dao;

import HappyFamily.structure.model.Family;

import java.io.IOException;
import java.util.List;

public interface FamilyDao {

     List<Family> getAllFamilies();

     Family getFamilyByIndex(int index);

     boolean deleteFamily(Family family);

     boolean addFamily(Family family);

     void saveFamily(Family family);

     void setFamilies(List<Family> families);

     void loadExternalData(String filepath);

     void saveExternalData(String filepath) throws IOException;
}
