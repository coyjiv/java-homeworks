package HappyFamily.structure.dao;

import HappyFamily.structure.model.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();
    private static final String DB_FILE_NAME = "families.ser";

    public CollectionFamilyDao() {
        loadData();
    }

    public void setFamilies(List<Family> families){
        this.families = families;
        saveData();
    }

    @Override
    public void loadExternalData(String filepath) {
        File file = new File(filepath);
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                Object readObject = ois.readObject();
                if (readObject instanceof List) {
                    families.clear();
                    families.addAll((List<Family>) readObject);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("File not found at: " + filepath);
        }
    }

    @Override
    public void saveExternalData(String filepath) throws IOException {
        File file = new File(filepath);
        if (!file.exists()) {
            file.createNewFile();
        }

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(families);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(Family family) {
        if(families.contains(family)){
            families.remove(family);
            saveData();
            return true;
        }
        return false;
    }

    @Override
    public boolean addFamily(Family family) {
        boolean result = families.add(family);
        if(result) saveData();
        return result;
    }

    @Override
    public void saveFamily(Family family) {
        if(families.contains(family)){
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
        saveData();
    }

    public void saveData(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DB_FILE_NAME))){
            oos.writeObject(families);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void loadData(){
        File file = new File(DB_FILE_NAME);
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                Object readObject = ois.readObject();
                if (readObject instanceof List) {
                    families.addAll((Collection<? extends Family>) readObject);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
