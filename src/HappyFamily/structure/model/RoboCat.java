package HappyFamily.structure.model;

import java.util.Set;

public class RoboCat extends Pet implements Foulable {
    private final Species species = Species.ROBO_CAT;

    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    @Override
    public void respond() {
        if(getFamily() != null && getFamily().getFather().getName().length()>0) {
            System.out.printf("Hi, owner. I am - %s. I missed you.", getFamily().getFather().getName());
        } else {
            System.out.println("...grrrrrrr....");
        }
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void foul() {
        System.out.println("I need to hide the cyber-traces...");
    }
}
