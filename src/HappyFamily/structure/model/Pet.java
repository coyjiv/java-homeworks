package HappyFamily.structure.model;

import java.io.Serializable;
import java.util.Set;

public abstract class Pet implements Serializable {
    private final Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    private Family family;

    public Pet(){

    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.family = family;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }



    public void eat(){
        System.out.println("I'm eating!");
    }

    public void setFamily(Family family){
        this.family = family;
    }

    public Family getFamily(){
        return family;
    }

    public abstract void respond();

    public abstract Species getSpecies();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    @Override
    public String toString() {
        return getSpecies()==Species.UNKNOWN?"Pet"+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}':getSpecies().name()+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

}
