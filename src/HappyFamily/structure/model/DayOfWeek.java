package HappyFamily.structure.model;

import java.io.Serializable;

public enum DayOfWeek implements Serializable {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
