package HappyFamily.structure.model;

import TaskPlanner.Schedule;

public final class Woman extends Human implements HumanCreator {
    public Woman() {
    }

    public Woman(String name, String surname, int year, int iq, Schedule schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public void makeup(){
        System.out.println("After doing makeup I look so much better!");
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    @Override
    public void greetPet(){
        if(getFamily() == null){
            System.out.println("I don't have a family :(");
            return;
        }
        if(getFamily().getPet() == null){
            System.out.println("My family don't own a pet :(");
            return;
        }
        System.out.printf("Hi, my sweet %s <3\n", getFamily().getPet().getNickname());
    }
}
