package HappyFamily.structure.model;

import TaskPlanner.Schedule;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Human implements Serializable {
    private Family family;
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Schedule schedule;

    public Human(){

    }

    public Human(String name, String surname, String birthDateStr, int iq) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthDate = LocalDate.parse(birthDateStr, formatter);
        LocalDateTime startOfDay = birthDate.atStartOfDay();
        this.birthDate = startOfDay.toInstant(ZoneOffset.UTC).toEpochMilli();
        this.iq = iq;
    }

    public Human(String name, String surname, int birthDate, int iq, Schedule schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, int birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public void describePet(){
        System.out.printf("I have %s, it's %s age old, he is %s.", family.getPet().getSpecies(), family.getPet().getAge(), family.getPet().getTrickLevel() > 50 ? "very tricky": "almost not tricky");
    }

    public String describeAge(){
        LocalDateTime birthTime =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate),
                        TimeZone.getDefault().toZoneId());
        return String.format("I am living for %s years, %s month, %s days",
                LocalDate.now().getYear() - birthTime.getYear(), (LocalDate.now().getYear() - birthTime.getYear())/12, TimeUnit.MILLISECONDS.toDays(birthDate));
    }

    public void greetPet(){
        System.out.printf("Hi, %s\n", family.getPet().getNickname());
    }

    public boolean feedPet(boolean hasTimeToFeedCome){
        System.out.println("(Should I feed the pet?)");
        if(!hasTimeToFeedCome){
            return false;
        }
        Random random = new Random();
        int randomChoice = random.nextInt(100)+1;
        if(randomChoice> family.getPet().getTrickLevel()){
            System.out.printf("Hmm... I need to feed the %s\n", family.getPet().getNickname());
            return true;
        }
        System.out.printf("I don't think that %s is hungry\n", family.getPet().getNickname());
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        return getFamily().equals(human.getFamily());
    }

    @Override
    public int hashCode() {
        return getFamily().hashCode();
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate),
                TimeZone.getDefault().toZoneId()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

}
