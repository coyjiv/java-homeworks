package HappyFamily.structure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Family implements Serializable {
    private final Woman mother;
    private final Man father;
    private List<Human> children;
    private List<Pet> pets = new ArrayList<>();

    public Family(Woman mother, Man father){
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.children = new ArrayList<>();

    }

    public Family(Woman mother, Man father, List<Pet> pets){
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pets = pets;
        this.children = new ArrayList<>();
    }

    public Woman getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public Pet getPet(int index){ return pets.get(index);}
    public Pet getPet(){ Random random = new Random(); return pets.get(random.nextInt(pets.size()));}

    public void addPet(Pet pet) {
        pets.add(pet);
        pet.setFamily(this);
    }

    public void addChild(Human child){
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(Human child){
        if(child.getFamily() == null || !children.contains(child)){
            return false;
        }
        child.setFamily(null);
        children.remove(child);
        return true;
    }

    public boolean deleteChild(int index){
        if(index>= children.size() || index<0){
            return false;
        }
        Human child = children.get(index);
        child.setFamily(null);
        children.remove(index);
        return true;
    }

    public int countFamily(){
        return 2 + children.size();
    }


    @Override
    public String toString() {
        return "family: \n" +
                "   mother:" + mother +
                "\n   father:" + father +
                "\n   children:" + children +
                "\n   pets:" + pets +
                '\n'+
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!getMother().equals(family.getMother())) return false;
        return getFather().equals(family.getFather());
    }

    @Override
    public int hashCode() {
        int result = getMother().hashCode();
        result = 31 * result + getFather().hashCode();
        return result;
    }

    public void prettyPrint() {
        System.out.println("family: \n" +
                "   mother:" + mother +
                "\n   father:" + father +
                "\n children:" + children +
                "\n pets:" + pets +
                '}');
    }
}
