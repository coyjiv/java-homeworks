package HappyFamily.structure.model;

import HappyFamily.structure.model.Family;
import HappyFamily.structure.model.Pet;
import HappyFamily.structure.model.Species;

import java.util.Set;

public class Fish extends Pet {
    private final Species species = Species.FISH;

    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish(String nickname) {
        super(nickname);
    }

    @Override
    public void respond() {
        System.out.println("I'm a fish...");
    }

    @Override
    public Species getSpecies() {
        return species;
    }
}
