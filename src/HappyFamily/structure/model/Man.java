package HappyFamily.structure.model;

import HappyFamily.structure.model.Human;
import TaskPlanner.Schedule;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, Schedule schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public void repairCar(){
        System.out.println("I will never understand how to fix this old pile of metal...");
    }

    @Override
    public void greetPet(){
        if(getFamily() == null){
            System.out.println("I don't have a family :(");
            return;
        }
        if(getFamily().getPet() == null){
            System.out.println("My family don't own a pet :(");
            return;
        }
        System.out.printf("Hi, %s\n", getFamily().getPet().getNickname());
    }
}
