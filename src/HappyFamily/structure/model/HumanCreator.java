package HappyFamily.structure.model;

import HappyFamily.structure.model.Family;
import HappyFamily.structure.model.Human;
import HappyFamily.structure.model.Man;
import HappyFamily.structure.model.Woman;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public interface HumanCreator {
    Random random = new Random();

    default int getIq(Family family){
        if(family == null){
            return -1;
        }
        return (int) Math.ceil((family.getFather().getIq() + family.getMother().getIq())/2);
    }

    default Human bornChild(Family family, String womanName, String manName,  int yearOfBirth){
        boolean isGirl = random.nextBoolean();
        String randomName = isGirl
                ? womanName
                : manName;
        if(isGirl){
            Human child = new Woman(randomName, family.getFather().getSurname(), yearOfBirth, getIq(family));
            family.addChild(child);
            return child;
        } else {
            Human child = new Man(randomName, family.getFather().getSurname(), yearOfBirth, getIq(family));
            family.addChild(child);
            return child;
        }

    }
}
