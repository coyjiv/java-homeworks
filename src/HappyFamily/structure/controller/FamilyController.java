package HappyFamily.structure.controller;

import HappyFamily.structure.model.*;
import HappyFamily.structure.service.FamilyService;
import HappyFamily.structure.service.IFamilyService;
import HappyFamily.structure.utils.ConsoleUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class FamilyController implements IFamilyService {
    private final FamilyService familyService;
    private final static Scanner scanner = new Scanner(System.in);
    public FamilyController(FamilyService familyService){
        this.familyService = familyService;
    }


    @Override
    public boolean editFamilyByIndex(Scanner scanner, int index) {
        return familyService.editFamilyByIndex(scanner, index);
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    @Override
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int memberCount) {
        return familyService.getFamiliesBiggerThan(memberCount);
    }

    @Override
    public List<Family> getFamiliesLessThan(int memberCount) {
        return familyService.getFamiliesLessThan(memberCount);
    }

    @Override
    public int countFamiliesWithMemberNumber(int memberCount) {
        return familyService.countFamiliesWithMemberNumber(memberCount);
    }

    @Override
    public void createNewFamily(Man father, Woman mother) {
        familyService.createNewFamily(father, mother);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    @Override
    public Family bornChild(Family family, String menName, String womenName) {
        return familyService.bornChild(family, menName, womenName);
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    @Override
    public boolean deleteAllChildrenOlderThan(int age) {
        return familyService.deleteAllChildrenOlderThan(age);
    }

    @Override
    public int count() {
        return familyService.count();
    }

    @Override
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }


    @Override
    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

    public void setFamilies(List<Family> families){
        familyService.setFamilies(families);
    }


    public void fillWithMockedData(){
        Man father1 = new Man("Valeriy", "Skorohod", 1990);
        Woman mother1 = new Woman("Luda", "Skorohod", 1993);
        createNewFamily(father1, mother1);
        addPet(0, new Dog("Striker", 5, 120, new HashSet<>(List.of("sleep", "eat", "run"))));
        adoptChild(getFamilyById(0), new Human("Alex", "Tur", 2004));


        Man father2 = new Man("Gregor", "Pschemyslavskiy", 1959);
        Woman mother2 = new Woman("Lida", "Pschemyslavska", 1964);
        createNewFamily(father2, mother2);
        addPet(1, new DomesticCat("Bober", 2, 30, new HashSet<>(List.of("sleep", "eat", "scream"))));
        bornChild(getFamilyById(1), "Krysztof", "Mira");

        Man father3 = new Man("Luigi", "Sarkotello", 1944);
        Woman mother3 = new Woman("Izabella", "Sarkotello", 1946);
        createNewFamily(father3, mother3);

        Man father4 = new Man("Dick", "Dale", 1937);
        Woman mother4 = new Woman("Lana", "Dale", 1946);
        createNewFamily(father4, mother4);
        bornChild(getFamilyById(3), "Jimmy", "Kim");
    }

    public void displayFamiliesWithHumanCountBiggerThanNum(){
            System.out.println("Enter a number : ");
            int num = ConsoleUtils.handleInputedNumber(scanner, "You've entered not a number, try again");
            System.out.println("Displaying families with member count greater than : " + num);
            ConsoleUtils.displayFoundList(getFamiliesBiggerThan(num));
    }

    private void displayFamiliesWithHumanCountLessThanNum() {
            System.out.println("Enter a number : ");
            int num = ConsoleUtils.handleInputedNumber(scanner, "You've entered not a number, try again");
            System.out.println("Displaying families with member count less than : " + num);
            ConsoleUtils.displayFoundList(getFamiliesLessThan(num));
    }

    private void displayFamiliesCountWithMemberCountEqualsNum(){
            int num = ConsoleUtils.handleInputedNumber(scanner, "You've entered not a number, try again");
            System.out.println("Displaying families with member count equals to : " + num);
            ConsoleUtils.displayFoundList(familyService.getFamiliesEqualsTo(num));
    }

    private void createAndStoreNewFamily(){
            System.out.println("Enter the name of mother for a new family ");
            String motherName = scanner.nextLine();
            System.out.println("Enter the surname of mother for a new family ");
            String motherSurname = scanner.nextLine();
            System.out.println("Enter the year of birth of mother for a new family ");
            int motherYearOfBirth = ConsoleUtils.handleInputedYear(scanner, "Entered value doesn't seem like year");
            System.out.println("Enter the iq of mother for a new family ");
            int mothersIq = ConsoleUtils.handleInputedIq(scanner, "Doesn't seem like a valid iq");
            Woman mother = new Woman(motherName, motherSurname, motherYearOfBirth, mothersIq);

        System.out.println("Enter the name of father for a new family ");
        String fatherName = scanner.nextLine();
        System.out.println("Enter the surname of father for a new family ");
        String fatherSurname = scanner.nextLine();
        System.out.println("Enter the year of birth of father for a new family ");
        int fatherYearOfBirth = ConsoleUtils.handleInputedYear(scanner, "Entered value doesn't seem like year");
        System.out.println("Enter the iq of father for a new family ");
        int fathersIq = ConsoleUtils.handleInputedIq(scanner, "Doesn't seem like a valid iq");
        Man father = new Man(fatherName, fatherSurname, fatherYearOfBirth, fathersIq);

        createNewFamily(father, mother);

        System.out.println("Family created successfully");

    }

    private void deleteFamilyByIndex(){
        System.out.print("Enter the index for family deletion : ");
        int index = ConsoleUtils.handleInputedNumber(scanner, "The provided value is not a number");
        if(!deleteFamilyByIndex(index)){
            System.out.println("The provided index is out of bounds");
        } else {
            System.out.println("The family with index " + index + " deleted successfully");
        }
    }

    private void editFamilyByIndex(){
        System.out.print("Enter the index for family edit : ");
        int index = ConsoleUtils.handleInputedNumber(scanner, "The provided value is not a number");
        editFamilyByIndex(scanner, index);
    }

    private void deleteAllChildrenOlderThan(){
        System.out.println("Enter the number under what children will be deleted");
        int number = ConsoleUtils.handleInputedNumber(scanner, "Not a number.");
        if(deleteAllChildrenOlderThan(number)){
            System.out.println("success");
        } else {
            System.out.println("nothing changed");
        }

    }

    private void loadData(){
        scanner.nextLine();
        System.out.println("Enter the filename if the saved file is inside the app folder, or the full path to the file of data");
        String filepath = ConsoleUtils.handleInputedString(scanner, "Doesn't look like a string");
        familyService.loadExternalData(filepath);
    }

    private void createExternalSave(){
        scanner.nextLine();
        System.out.println("Enter the filename for save the state of the family");
        String filepath = ConsoleUtils.handleInputedString(scanner, "Doesn't look like a string");
        try {
            familyService.saveExternalData(filepath);
        } catch (IOException e){
            e.printStackTrace();
        }
    }



    public void menu() {

        System.out.println("Greetings, to continue, choose the option from the list below: ");

        String menu = """
                 1. Fill in with test data
                 2. Display the entire list of families
                 3. Display the list of families, where human members count greater than entered
                 4. Display the list of families, where human members count less than entered
                 5. Count the number of families, where number of members equals entered
                 6. Create a new family
                 7. Delete family by index
                 8. Edit family by index
                 9. Display all children older than age
                 10. Load external data
                 11. Save external data

                 0./exit Exit from app
                
                """;

        String option = null;

        while (true){
            System.out.println(menu);
            System.out.print("Your option: ");

            if (!scanner.hasNext()){
                System.out.println("Enter a valid option! You've entered : " + option);
                scanner.next();
                continue;
            }

            option = scanner.next();

            switch (option) {
                case "1" -> fillWithMockedData();
                case "2" -> displayAllFamilies();
                case "3" -> displayFamiliesWithHumanCountBiggerThanNum();
                case "4" -> displayFamiliesWithHumanCountLessThanNum();
                case "5" -> displayFamiliesCountWithMemberCountEqualsNum();
                case "6" -> createAndStoreNewFamily();
                case "7" -> deleteFamilyByIndex();
                case "8" -> editFamilyByIndex();
                case "9" -> deleteAllChildrenOlderThan();
                case "10" -> loadData();
                case "11" -> createExternalSave();
                case "0" -> System.exit(0);
                default -> System.out.println("This option doesn't exist");
            }

        }

    }


}
