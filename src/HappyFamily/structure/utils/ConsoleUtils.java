package HappyFamily.structure.utils;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class ConsoleUtils {
    public static void displayFoundList(List list){
        if(list.size()>0){
            list.forEach(System.out::println);
        } else {
            System.out.println("No results");
        }
    }
    public static int handleInputedNumber(Scanner scanner, String exception){
        try{
            return scanner.nextInt();
        }
        catch (NumberFormatException e){
            System.out.println(exception);
            return -1;
        }
    }
    public static int handleInputedYear(Scanner scanner, String exception){
        try{
            int year = scanner.nextInt();
            if(year>1500 && year < LocalDate.now().getYear()){
                return year;
            } else {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException e){
            System.out.println(exception);
            return -1;
        }
    }

    public static String handleInputedString(Scanner scanner, String exception){
        try{
            return scanner.nextLine();
        }
        catch (NumberFormatException e){
            System.out.println(exception);
            return "";
        }
    }

    public static int handleInputedIq(Scanner scanner, String exception) {
        try{
            int iq = scanner.nextInt();
            if(iq>0 && iq<1000){
                return iq;
            } else {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException e){
            System.out.println(exception);
            return 1;
        }
    }
}
