package HappyFamily.structure.utils;

public class FamilyOverflowException extends RuntimeException{
    private final int number;
    public int getNumber(){return number;}
    public FamilyOverflowException(String message, int num){
        super(message);
        number=num;
    }
}
