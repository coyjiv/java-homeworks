import TaskPlanner.TaskPlanner;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hi, enter the number for the homework:");
        if (!scanner.hasNextInt()) {
            System.out.println("Invalid input. Please enter a valid integer.");
            System.exit(0);
        }
        int choice = scanner.nextInt();

        switch (choice){
            case 1:
                Numbers.main();
                break;
            case 2:
                AreaShooting.main();
                break;
            case 3:
                TaskPlanner taskPlanner = new TaskPlanner();
                taskPlanner.start();
                break;
            default:
                System.out.println("Entered homework don't exist...yet");
                System.exit(0);
        }
    }
}
